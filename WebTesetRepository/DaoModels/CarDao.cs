﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularTestRepository.DaoModels
{
    public class CarDao
    {
        public int? Id { get; set; }

      
        public string Model { get; set; }

        public string Comment { get; set; }
        public long Price { get; set; }
        public DateTime Date { get; set; }

        public bool Deleted { get; set; }
    }
}
