﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularTestRepository.API;
using AngularTestRepository.DaoModels;

namespace AngularTestRepository.Implementation
{
    public sealed class MotoRepository : IMotoRepository
    {

        private static List<MotoDao> _motos;
        private static int _idCounter;
        private static readonly object ThisLock = new object();

        static MotoRepository()
        {
            Random rand = new Random();
            _motos = new List<MotoDao>();
            for (int i = 0; i < 100; i++)
            {
                _motos.Add(new MotoDao
                {
                    Id = ++_idCounter,
                    Model = "Model" + _idCounter,
                    Comment = "Comment" + _idCounter,
                    Price = _idCounter,
                    Date = DateTime.Now

                });
            }

        }

        #region CRUD
        public MotoDao[] GetAllMotos()
        {
            return _motos.Where(x => !x.Deleted).ToArray();
        }

        public MotoDao GetMotoById(int? motoId)
        {
            MotoDao moto = _motos.SingleOrDefault(x => x.Id == motoId);
            if (moto != null && !moto.Deleted)
            {
                return moto;
            }
            return null;
        }

        public bool AddNewMoto(MotoDao moto)
        {
            if (moto.Id == 0)
            {
                lock (ThisLock)
                {
                    moto.Id = ++_idCounter;

                    _motos.Add(moto);
                }
                return true;
            }
            return false;
        }

        public bool UpdateMoto(MotoDao moto)
        {
            MotoDao oldMoto = GetMotoById(moto.Id);
            if (oldMoto != null && !oldMoto.Deleted)
            {
                oldMoto.Model = moto.Model;
                oldMoto.Comment = moto.Comment;
                oldMoto.Price = moto.Price;
                oldMoto.Date = moto.Date;

                return true;
            }
            return false;
        }

        public bool RemoveMotoById(int motoId)
        {
            lock (ThisLock)
            {
                MotoDao moto = GetMotoById(motoId);
                if (moto != null && !moto.Deleted)
                {
                    moto.Deleted = true;
                    return true;
                }
            }
            return false;
        }
        #endregion
    }
}