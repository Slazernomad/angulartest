﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using AngularTestRepository.API;
using AngularTestRepository.DaoModels;

namespace AngularTestRepository.Implementation
{
   public sealed class CarRepository: ICarRepository
    {
       
            private static List<CarDao> _cars;
            private static int _idCounter;
            private static readonly object ThisLock = new object();

            static CarRepository()
            {
                Random rand = new Random();
                _cars = new List<CarDao>();
                for (int i = 0; i < 100; i++)
                {
                    _cars.Add(new CarDao
                    {
                        Id = ++_idCounter,
                        Model = "Model" + _idCounter,
                        Comment = "Comment" + _idCounter,
                        Price = _idCounter,
                        Date = DateTime.Now
                    });
                }
             
            }
            #region CRUD
            public CarDao[] GetAllCars()
            {
                return _cars.Where(x => !x.Deleted).ToArray();
            }

            public CarDao GetCarById(int? carId)
            {
            CarDao car = _cars.SingleOrDefault(x => x.Id == carId);
                if (car != null && !car.Deleted)
                {
                    return car;
                }
                return null;
            }

            public bool AddNewCar(CarDao car)
            {
                if (car.Id == 0)
                {
                    lock (ThisLock)
                    {
                    car.Id = ++_idCounter;
                    
                        _cars.Add(car);
                    }
                    return true;
                }
                return false;
            }

            public bool UpdateCar(CarDao car)
            {
            CarDao oldCar = GetCarById(car.Id);
                if (oldCar != null && !oldCar.Deleted)
                {
                oldCar.Model = car.Model;
                oldCar.Comment = car.Comment;
                oldCar.Price = car.Price;
                    oldCar.Date = car.Date;
                    
                return true;
                }
                return false;
            }

            public bool RemoveCarById(int carId)
            {
                lock (ThisLock)
                {
                    CarDao car = GetCarById(carId);
                    if (car != null && !car.Deleted)
                    {
                        car.Deleted = true;
                        return true;
                    }
                }
                return false;
            }
            #endregion
        }
    }


          
