﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularTestRepository.DaoModels;

namespace AngularTestRepository.API
{
    public interface IMotoRepository
    {
        MotoDao[] GetAllMotos();
       MotoDao GetMotoById(int? id);
        bool AddNewMoto(MotoDao moto);
        bool UpdateMoto(MotoDao moto);
        bool RemoveMotoById(int id);
    }
}
