﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularTestRepository.DaoModels;

namespace AngularTestRepository.API
{
    public interface ICarRepository
    {
        
        
            CarDao[] GetAllCars();
            CarDao GetCarById(int? id);
            bool AddNewCar(CarDao car);
            bool UpdateCar(CarDao car);
            bool RemoveCarById(int id);


          

        }
    }

