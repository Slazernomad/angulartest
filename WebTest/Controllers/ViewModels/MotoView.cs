﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularTest.Controllers.ViewModels
{
    public class MotoView
    {
        public int? Id { get; set; }


        public string Model { get; set; }

        public string Comment { get; set; }
        public long Price { get; set; }
        public DateTime Date { get; set; }
    }
}