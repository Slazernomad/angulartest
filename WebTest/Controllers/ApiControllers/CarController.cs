﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Web.Http;
using Monitor.Controllers.Templates;
using AngularTest.Controllers.ViewModels;
using AngularTest.Services.API;
using AngularTest.Services.DtoModels;

namespace AngularTest.Controllers.ApiControllers
{
    public class CarController : MainApiController
    {
        private readonly ICarService _service;

        public CarController(ICarService service)
        {
            _service = service;
        }

        #region CRUD

        [HttpGet]
        public IEnumerable<CarView> GetAllCars()
        {
            IEnumerable<CarDto> cars = _service.GetAllCars();
            return Map<IEnumerable<CarView>, IEnumerable<CarDto>>(cars);
        }

        [HttpGet]
        public CarView GetCarById(int carId)
        {
            CarDto car = _service.GetCarById(carId);
            
           var b = Map<CarView, CarDto>(car);
            return Map<CarView, CarDto>(car);
        }

        [HttpPost]
        public HttpResponseMessage AddNewCar(CarView car)
        {
            CarDto newCar = Map<CarDto, CarView>(car);
            bool result = _service.AddNewCar(newCar);
            return HttpCode(result);
        }

        [HttpPut]
        public HttpResponseMessage UpdateCar(CarView car)
        {
            CarDto updCar = Map<CarDto, CarView>(car);
            bool result = _service.UpdateCar(updCar);
            return HttpCode(result);
        }

        [HttpDelete]
        public HttpResponseMessage RemoveCarById(int carId)
        {
            bool result = _service.RemoveCarById(carId);
            return HttpCode(result);
        }

        #endregion
    }
}
