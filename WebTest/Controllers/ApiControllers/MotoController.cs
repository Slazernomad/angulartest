﻿using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using Monitor.Controllers.Templates;
using AngularTest.Controllers.ViewModels;
using AngularTest.Services.API;
using AngularTest.Services.DtoModels;

namespace AngularTest.Controllers.ApiControllers
{
    public class MotoController : MainApiController
    {
        private readonly IMotoService _service;

        public MotoController(IMotoService service)
        {
            _service = service;
        }

        #region CRUD

        [HttpGet]
        public IEnumerable<MotoView> GetAllMotos()
        {
            IEnumerable<MotoDto> motos = _service.GetAllMotos();
            return Map<IEnumerable<MotoView>, IEnumerable<MotoDto>>(motos);
        }

        [HttpGet]
        public MotoView GetMotoById(int motoId)
        {
            MotoDto moto = _service.GetMotoById(motoId);

            var b = Map<MotoView, MotoDto>(moto);
            return Map<MotoView, MotoDto>(moto);
        }

        [HttpPost]
        public HttpResponseMessage AddNewMoto(MotoView moto)
        {
            MotoDto newMoto = Map<MotoDto, MotoView>(moto);
            bool result = _service.AddNewMoto(newMoto);
            return HttpCode(result);
        }

        [HttpPut]
        public HttpResponseMessage UpdateMoto(MotoView moto)
        {
            MotoDto updMoto = Map<MotoDto, MotoView>(moto);
            bool result = _service.UpdateMoto(updMoto);
            return HttpCode(result);
        }

        [HttpDelete]
        public HttpResponseMessage RemoveMotoById(int motoId)
        {
            bool result = _service.RemoveMotoById(motoId);
            return HttpCode(result);
        }

        #endregion
    }
}
