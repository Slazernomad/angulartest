﻿using System.Web.Http;
using System.Web.Mvc;
using LightInject;
using AngularTest.Services.API;
using AngularTest.Services.Implementation;
using AngularTestRepository.API;
using AngularTestRepository.Implementation;

namespace AngularTest
{
    public class LightInject
    {
        public static void Initialize()
        {
            var container = new ServiceContainer();

            container.RegisterControllers();
            container.RegisterApiControllers();
            //register other services

            container.EnablePerWebRequestScope();
            container.EnableWebApi(GlobalConfiguration.Configuration);

            // Service injection.
            //container.Register<IStoreService, StoreService>();
            container.Register<ICarService, CarService>();
            container.Register<IMotoService, MotoService>();

            // Repository injection.
            //container.Register<IStoreRepository, StoreRepository>();
            container.Register<ICarRepository, CarRepository>();
            container.Register<IMotoRepository, MotoRepository>();

            DependencyResolver.SetResolver(container);
        }
    }
}