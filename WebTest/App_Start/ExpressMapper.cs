﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExpressMapper;
using AngularTestRepository.DaoModels;
using AngularTest.Controllers.ViewModels;
using AngularTest.Services.DtoModels;

namespace AngularTest
{
    public class ExpressMapper
    {
        public static void MappingRegistration()
        {
            Mapper.Register<CarView, CarDto>();
            Mapper.Register<CarDto, CarView>();
            Mapper.Register<CarDto, CarDao>();
            Mapper.Register<CarDao, CarDto>();

            Mapper.Register<MotoView, MotoDto>();
            Mapper.Register<MotoDto, MotoView>();
            Mapper.Register<MotoDto, MotoDao>();
            Mapper.Register<MotoDao, MotoDto>();



            Mapper.Compile();
        }
    }

   
}