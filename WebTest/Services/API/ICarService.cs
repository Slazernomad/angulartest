﻿using System.Collections.Generic;

using AngularTest.Services.DtoModels;

namespace AngularTest.Services.API
{
   public interface ICarService
    {
        IEnumerable<CarDto> GetAllCars();
        CarDto GetCarById(int id);
        bool AddNewCar(CarDto car);
        bool UpdateCar(CarDto car);
        bool RemoveCarById(int id);

    }
}
