﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularTestRepository.DaoModels;
using AngularTest.Services.DtoModels;

namespace AngularTest.Services.API
{
   public interface IMotoService
    {
        IEnumerable<MotoDto> GetAllMotos();
        MotoDto GetMotoById(int id);
        bool AddNewMoto(MotoDto moto);
        bool UpdateMoto(MotoDto moto);
        bool RemoveMotoById(int id);
    }
}
