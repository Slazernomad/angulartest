﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AngularTestRepository.API;
using AngularTestRepository.DaoModels;
using AngularTest.Services.API;
using AngularTest.Services.DtoModels;

namespace AngularTest.Services.Implementation
{
    public sealed class MotoService : GenericService, IMotoService
    {

        private readonly IMotoRepository _motoRepository;

        public MotoService(IMotoRepository motoRepository)
        {
            _motoRepository = motoRepository;
        }
        public IEnumerable<MotoDto> GetAllMotos()
        {
            IEnumerable<MotoDao> motos = _motoRepository.GetAllMotos();

            IEnumerable<MotoDto> motosDto = Map<IEnumerable<MotoDto>, IEnumerable<MotoDao>>(motos);
            return motosDto;
        }

        public MotoDto GetMotoById(int motoId)
        {

            MotoDao moto = _motoRepository.GetMotoById(motoId);
            MotoDto motoDto = Map<MotoDto, MotoDao>(moto);
            return motoDto;
        }

        public bool AddNewMoto(MotoDto moto)
        {
            MotoDao motoDao = Map<MotoDao, MotoDto>(moto);
            bool result = _motoRepository.AddNewMoto(motoDao);
            return result;
        }

        public bool UpdateMoto(MotoDto moto)
        {
            MotoDao motoDao = Map<MotoDao, MotoDto>(moto);
            bool result = _motoRepository.UpdateMoto(motoDao);
            return result;
        }

        public bool RemoveMotoById(int motoId)
        {
            bool result = _motoRepository.RemoveMotoById(motoId);
            return result;
        }
    }
}
