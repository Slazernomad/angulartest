﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AngularTestRepository.API;
using AngularTestRepository.DaoModels;
using AngularTest.Services.API;
using AngularTest.Services.DtoModels;

namespace AngularTest.Services.Implementation
{
    public sealed class CarService: GenericService, ICarService
    {
      
            private readonly ICarRepository _carRepository;

        public CarService(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }
        public IEnumerable<CarDto> GetAllCars()
            {
                IEnumerable<CarDao> cars = _carRepository.GetAllCars();

                IEnumerable<CarDto> carsDto = Map<IEnumerable<CarDto>, IEnumerable<CarDao>>(cars);
                return carsDto;
            }

            public CarDto GetCarById(int carId)
            {

            CarDao car = _carRepository.GetCarById(carId);
           CarDto carDto = Map<CarDto, CarDao>(car);
                return carDto;
            }

            public bool AddNewCar(CarDto car)
            {
            CarDao carDao = Map<CarDao, CarDto>(car);
                bool result = _carRepository.AddNewCar(carDao);
                return result;
            }

            public bool UpdateCar(CarDto car)
            {
            CarDao carDao = Map<CarDao, CarDto>(car);
                bool result = _carRepository.UpdateCar(carDao);
                return result;
            }

            public bool RemoveCarById(int carId)
            {
                bool result = _carRepository.RemoveCarById(carId);
                return result;
            }
        }
    }

